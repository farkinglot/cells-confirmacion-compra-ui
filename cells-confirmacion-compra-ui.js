{
  const {
    html,
  } = Polymer;
  /**
    `<cells-confirmacion-compra-ui>` Description.

    Example:

    ```html
    <cells-confirmacion-compra-ui></cells-confirmacion-compra-ui>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-confirmacion-compra-ui | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsConfirmacionCompraUi extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-confirmacion-compra-ui';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="cells-confirmacion-compra-ui-styles cells-confirmacion-compra-ui-shared-styles"></style>
      <slot></slot>
      
          <div>Se realizó tu pedido exitosamente</div>
          <fieldset class='container'>
            <div>
              <label><b>FECHA:</b></label>
            </div>
            <div>
              <label><b>HORA ENTREGA:</b></label>
            </div>
            <div>
              <label><b>PEDIDO:</b></label>
            </div>
            <div>
              <label><b>CANTIDAD:</b></label>
            </div>
            <div>
              <label><b>PRECIO:</b></label>
            </div>
            <div>
              <label><b>FECHA OPERACIÓN:</b></label>
            </div>
          </fieldset>
      
      `;
    }
  }

  customElements.define(CellsConfirmacionCompraUi.is, CellsConfirmacionCompraUi);
}